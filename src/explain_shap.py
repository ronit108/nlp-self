
from notebooks import bert_layer
import tensorflow as tf

inp = tf.keras.Input(shape =(1,),dtype=tf.string)
encoder = bert_layer.BertLayer(bert_path="C:/Users/ronisaha/PycharmProjects/BertClassification2/models/uncased_L-12_H-768_A-12/model.ckpt-1",seq_len=48,tune_embeddings=False,do_preprocessing=True,pooling='cls',n_tune_layers=3, verbose=False)

pred = tf.keras.layers.Dense(1, activation='sigmoid')(encoder(inp))

model = tf.keras.models.Model(inputs=[inp], outputs=[pred])

model.summary()

model.compile(
      optimizer=tf.keras.optimizers.Adam(learning_rate=1e-5, ),
      loss="binary_crossentropy",
      metrics=["accuracy"])

