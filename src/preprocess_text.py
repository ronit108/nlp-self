import numpy as np
import pandas as pd
import re
import gc
import os
import shap
print(os.listdir("C:/Users/ronisaha/PycharmProjects/BertClassification2"))
import fileinput
import string
import tensorflow as tf
from tensorflow.keras.preprocessing import text
import zipfile
import datetime
import sys
from tqdm  import tqdm
tqdm.pandas()
from nltk.tokenize import wordpunct_tokenize
from nltk.corpus import stopwords
from nltk.stem.snowball import SnowballStemmer
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.preprocessing import LabelEncoder
from sklearn.metrics import accuracy_score
from sklearn.metrics import f1_score, roc_auc_score

from keras.preprocessing.text import Tokenizer
from keras.preprocessing.sequence import pad_sequences
from keras.models import Sequential
from keras.layers import Dense, Flatten, LSTM, Conv1D, MaxPooling1D, Dropout, Activation
from keras.layers.embeddings import Embedding
from sklearn.metrics import classification_report
from notebooks import modlin
from bs4 import BeautifulSoup
import spacy
import unidecode
from word2number import w2n

import os
from mlflow import log_metric, log_param, log_artifact



log_metric("foo", 1)

nlp = spacy.load('de_core_news_sm')
# exclude words from spacy stopwords list
deselect_stop_words = ['nicht', 'nein','kein','ja','nie']
for w in deselect_stop_words:
    nlp.vocab[w].is_stop = False
def remove_special_characters(dataframe):
    #self.log.info("Removing special characters from dataframe")
    no_special_characters = dataframe.replace(r'[^A-Za-z0-9 ]+', '', regex=True)
    return no_special_characters

def strip_html_tags(text):
    """remove html tags from text"""
    soup = BeautifulSoup(text, "html.parser")
    stripped_text = soup.get_text(separator=" ")
    return stripped_text

def remove_accented_chars(text):
    """remove accented characters from text, e.g. café"""
    text = unidecode.unidecode(text)
    return text

def remove_whitespace(text):
    """remove extra whitespaces from text"""
    text = text.strip()
    return " ".join(text.split())


def text_preprocessing(text, accented_chars=True, contractions=True,
                       convert_num=True, extra_whitespace=True,
                       lemmatization=True, lowercase=True, punctuations=True,
                       remove_html=True, remove_num=True, special_chars=True,
                       stop_words=True):
    """preprocess text with default option set to true for all steps"""
    if remove_html == True:  # remove html tags
        text = strip_html_tags(text)
    if extra_whitespace == True:  # remove extra whitespaces
        text = remove_whitespace(text)
#    if accented_chars == True:  # remove accented characters
 #       text = remove_accented_chars(text)
#    if contractions == True:  # expand contractions
#        text = expand_contractions(text)
    if lowercase == True:  # convert all characters to lowercase
        text = text.lower()

    doc = nlp(text)  # tokenise text

    clean_text = []

    for token in doc:
        flag = True
        edit = token.text
        # remove stop words
        if stop_words == True and token.is_stop and token.pos_ != 'NUM':
            flag = False
        # remove punctuations
        if punctuations == True and token.pos_ == 'PUNCT' and flag == True:
            flag = False
        # remove special characters
        if special_chars == True and token.pos_ == 'SYM' and flag == True:
            flag = False
        # remove numbers
        if remove_num == True and (token.pos_ == 'NUM' or token.text.isnumeric()) \
                and flag == True:
            flag = False
        # convert number words to numeric numbers
        if convert_num == True and token.pos_ == 'NUM' and flag == True:
            edit = w2n.word_to_num(token.text)
        # convert tokens to base form
        elif lemmatization == True and token.lemma_ != "-PRON-" and flag == True:
            edit = token.lemma_
        # append tokens edited and not removed to list
        if edit != "" and flag == True:
            clean_text.append(edit)
        else:
            token.lower_

    return clean_text


df = pd.read_csv('C:/Users/ronisaha/PycharmProjects/BertClassification2/data/train.csv',delimiter = ';',header = None,encoding = "utf-8")
df.rename(columns={0: 'Context',1:'Class'}, inplace=True)
print('len-----',len(df))
for i in range(0,len(df)):
    unclean_text = df.iloc[i]['Context']
    clean_text = text_preprocessing(unclean_text)
    df.iloc[i]['Context'] = " ".join(str(x) for x in clean_text)

#print(df[1:2])
#unclean_text = df.iloc[2]['Context']
#print(df.iloc[2]['Context'])
#clean_text = text_preprocessing(unclean_text)

#df.iloc[2]['Context'] = " ".join(str(x) for x in clean_text)
#print(df.iloc[2]['Context'])
from notebooks import modlin
#import notebooks.modlin
import notebooks.optimization
from notebooks import run_clas
#import notebooks.run_clas
import notebooks.tes
folder = 'C:/Users/ronisaha/PycharmProjects/BertClassification2/models'
BERT_MODEL = 'uncased_L-12_H-768_A-12' #uncased_L-12_H-768_A-12
BERT_PRETRAINED_DIR = f'{folder}/uncased_L-12_H-768_A-12' #cased_L-24_H-1024_A-16
OUTPUT_DIR = f'{folder}/outputs/6'
print(f'>> Model output directory: {OUTPUT_DIR}')
print(f'>>  BERT pretrained directory: {BERT_PRETRAINED_DIR}')

#one_hot = pd.get_dummies(df["Class"])
#df.drop(['Class'],axis=1,inplace=True)
#df = pd.concat([df,one_hot],axis=1)
#print(df.head())
from sklearn.model_selection import train_test_split

df2 = pd.DataFrame()
df2["Context"] = df["Context"]
y= df["Class"]
df2["Class"] = LabelEncoder().fit_transform(y)
#df2 = df2[:5000]
#df2 = remove_special_characters(df2)
print(df2.head())
X_train, X_test, y_train, y_test = train_test_split(df2["Context"].values, df2["Class"].values, test_size=0.25, random_state=42)




def create_examples(lines, set_type, labels=None):
#Generate data for the BERT model
    guid = f'{set_type}'
    examples = []
    if guid == 'train':
        for line, label in zip(lines, labels):
            text_a = line
            label = str(label)
            examples.append(
              run_clas.InputExample(guid=guid, text_a=text_a, text_b=None, label=label))
    else:
        for line in lines:
            text_a = line
            label = '0'
            examples.append(
              run_clas.InputExample(guid=guid, text_a=text_a, text_b=None, label=label))
    return examples

# Model Hyper Parameters
TRAIN_BATCH_SIZE = 32
EVAL_BATCH_SIZE = 8
LEARNING_RATE = 1e-5
NUM_TRAIN_EPOCHS = 3.0
WARMUP_PROPORTION = 0.1
MAX_SEQ_LENGTH = 50
# Model configs
SAVE_CHECKPOINTS_STEPS = 1 #if you wish to finetune a model on a larger dataset, use larger interval
# each checpoint weights about 1,5gb
ITERATIONS_PER_LOOP = 100000
NUM_TPU_CORES = 8
VOCAB_FILE = os.path.join(BERT_PRETRAINED_DIR, 'vocab1.txt')
CONFIG_FILE = os.path.join(BERT_PRETRAINED_DIR, 'bert_config.json')
INIT_CHECKPOINT = os.path.join(BERT_PRETRAINED_DIR, 'model.ckpt-1')#model.ckpt-2  bert-base-german-cased model.ckpt-351 ----391 best  model.ckpt-1
DO_LOWER_CASE = BERT_MODEL.startswith('uncased')

label_list = [str(num) for num in range(2)]
tokenizer = notebooks.tes.FullTokenizer(vocab_file=VOCAB_FILE, do_lower_case=DO_LOWER_CASE)
train_examples = create_examples(X_train, 'train', labels=y_train)

tpu_cluster_resolver = None #Since training will happen on GPU, we won't need a cluster resolver
#TPUEstimator also supports training on CPU and GPU. You don't need to define a separate tf.estimator.Estimator.
run_config = tf.contrib.tpu.RunConfig(
    cluster=tpu_cluster_resolver,
    model_dir=OUTPUT_DIR,
    save_checkpoints_steps=SAVE_CHECKPOINTS_STEPS,
    tpu_config=tf.contrib.tpu.TPUConfig(
        iterations_per_loop=ITERATIONS_PER_LOOP,
        num_shards=NUM_TPU_CORES,
        per_host_input_for_training=tf.contrib.tpu.InputPipelineConfig.PER_HOST_V2))

num_train_steps = int(
    len(train_examples) / TRAIN_BATCH_SIZE * NUM_TRAIN_EPOCHS)
num_warmup_steps = int(num_train_steps * WARMUP_PROPORTION)

model_fn = run_clas.model_fn_builder(
    bert_config=modlin.BertConfig.from_json_file(CONFIG_FILE),
    num_labels=len(label_list),
    init_checkpoint=INIT_CHECKPOINT,
    learning_rate=LEARNING_RATE,
    num_train_steps=num_train_steps,
    num_warmup_steps=num_warmup_steps,
    use_tpu=False, #If False training will fall on CPU or GPU, depending on what is available
    use_one_hot_embeddings=True)

estimator = tf.contrib.tpu.TPUEstimator(
    use_tpu=False, #If False training will fall on CPU or GPU, depending on what is available
    model_fn=model_fn,
    config=run_config,
    train_batch_size=TRAIN_BATCH_SIZE,
    eval_batch_size=EVAL_BATCH_SIZE)


"""Training BERT using last saved checkpoint"""
print('Please wait...')
#train_features = run_clas.convert_examples_to_features(
#    train_examples, label_list, MAX_SEQ_LENGTH, tokenizer)
#print('>> Started training at {} '.format(datetime.datetime.now()))
#print('  Num examples = {}'.format(len(train_examples)))
#print('  Batch size = {}'.format(TRAIN_BATCH_SIZE))
#tf.compat.v1.logging.info("  Num steps = %d", num_train_steps)
#train_input_fn = run_clas.input_fn_builder(
 #  features=train_features,
 #   seq_length=MAX_SEQ_LENGTH,
 #   is_training=True,
 #  drop_remainder=True)
#estimator.train(input_fn=train_input_fn, max_steps=num_train_steps)
#print('>> Finished training at {}'.format(datetime.datetime.now()))




def input_fn_builder(features, seq_length, is_training, drop_remainder):
  """Creates an `input_fn` closure to be passed to TPUEstimator."""

  all_input_ids = []
  all_input_mask = []
  all_segment_ids = []
  all_label_ids = []

  for feature in features:
    all_input_ids.append(feature.input_ids)
    all_input_mask.append(feature.input_mask)
    all_segment_ids.append(feature.segment_ids)
    all_label_ids.append(feature.label_id)

  def input_fn(params):
    """The actual input function."""
    print(params)
    batch_size = 500

    num_examples = len(features)

    d = tf.data.Dataset.from_tensor_slices({
        "input_ids":
            tf.constant(
                all_input_ids, shape=[num_examples, seq_length],
                dtype=tf.int32),
        "input_mask":
            tf.constant(
                all_input_mask,
                shape=[num_examples, seq_length],
                dtype=tf.int32),
        "segment_ids":
            tf.constant(
                all_segment_ids,
                shape=[num_examples, seq_length],
                dtype=tf.int32),
        "label_ids":
            tf.constant(all_label_ids, shape=[num_examples], dtype=tf.int32),
    })

    if is_training:
      d = d.repeat()
      d = d.shuffle(buffer_size=100)

    d = d.batch(batch_size=batch_size, drop_remainder=drop_remainder)
    return d

  return input_fn


predict_examples = create_examples(X_test, 'test')

predict_features = run_clas.convert_examples_to_features(
    predict_examples, label_list, MAX_SEQ_LENGTH, tokenizer)

predict_input_fn = input_fn_builder(
    features=predict_features,
    seq_length=MAX_SEQ_LENGTH,
    is_training=False,
    drop_remainder=False)

result = estimator.predict(input_fn=predict_input_fn)
#predictions = [p['probabilities'][0] for p in estimator.predict(input_fn=predict_input_fn)]
#print(np.array(predictions))
preds = []
for prediction in result:
      preds.append(np.argmax(prediction['probabilities']))

from sklearn.metrics import accuracy_score
print("Accuracy of BERT is:",accuracy_score(y_test,preds))

log_metric("acc", accuracy_score(y_test,preds))

print(classification_report(y_test,preds))



